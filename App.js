import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Cat from './components/cat';
import MyApp from './components/myapp';
import MyFlatList from './components/flatlist';

export default function App() {
  return (
    <View style={styles.container}>
      <MyApp />
      <Cat />
      <Cat />
      <MyFlatList />
      <Text>Hello people</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
