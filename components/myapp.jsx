import React from 'react';
import { View, Text, Image, ScrollView, TextInput } from 'react-native';

const myapp = () => {
    return (
        <ScrollView>
            <Text>Some text</Text>
            <View>
                <Text>Some more text</Text>
                <Image
                    source={{
                        uri: 'https://reactnative.dev/docs/assets/p_cat2.png',
                    }}
                    style={{ width: 10, height: 10 }}
                />
            </View>
            <TextInput
                style={{
                    height: 10,
                    borderColor: 'gray',
                    borderWidth: 1
                }}
                defaultValue="You can type in me"
            />
        </ScrollView>
    );
}

export default myapp;