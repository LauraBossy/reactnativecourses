import { View, Text, TextInput, Image } from 'react-native'
import React from 'react'

const Cat = ({ name }) => {
    // const name = 'Mario'
    const Presentation = (work, friend) => {
        return 'With my friend ' + friend + ' I work as a ' + work;
    }
    return (
        <View>
            <Text> Hello it's is me, {name} !</Text>
            <Text> {Presentation("plomber", "Luigi")} </Text>
            <TextInput
                style={{
                    height: 10,
                    borderColor: 'gray',
                    borderWidth: 1
                }}
                defaultValue="Name me!"
            />
            <Image
                source={{ uri: "https://reactnative.dev/docs/assets/p_cat1.png" }}
                style={{ width: 10, height: 10 }}
            />
        </View>
    )
}

const Cafe = () => {
    return (
        <View>
            <Cat name="Jellylorum" />
            <Cat name="Spot" />
        </View>
    );
}

export default Cafe;