import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
const nameList = require('../data/nameList.json');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
});

const FlatListBasics = () => {
    return (
        <FlatList
            data={nameList}
            renderItem={({ item }) => <Text>{item.key}</Text>}
        />
    );
}

export default FlatListBasics;